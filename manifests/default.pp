user { 'play': 
	ensure   => present,
	password => '$1$xyz$5IIMcPgJhe2zXvOmKgfZg1',
	home => '/opt/play',
	shell => '/bin/bash',
	groups => ['audio'],
	managehome => true
}
file { '/opt/play':
	ensure => 'directory',
	owner  => 'play',
	group  => 'play',
	mode   => '0750',
}
package { 'xinit':
	provider => 'apt',
	ensure  => installed,
}
package { 'vim':
	provider => 'apt',
	ensure  => installed,
}
package { 'ruby-dev':
	provider => 'apt',
	ensure  => installed,
}
package { 'build-essential':
	provider => 'apt',
	ensure  => installed,
}
package { 'libsdl2-dev':
	provider => 'apt',
	ensure => installed,
}
package { 'libsdl2-ttf-dev':
	provider => 'apt',
	ensure => installed,
}
package { 'libpango1.0-dev':
	provider => 'apt',
	ensure => installed,
}
package { 'libgl1-mesa-dev':
	provider => 'apt',
	ensure => installed,
}
package { 'libopenal1':
	provider => 'apt',
	ensure => installed,
}
package { 'libopenal-dev':
	provider => 'apt',
	ensure => installed,
}
package { 'libsndfile-dev':
	provider => 'apt',
	ensure => installed,
}
package { 'libmpg123-dev':
	provider => 'apt',
	ensure => installed,
}
package { 'libgmp-dev':
	provider => 'apt',
	ensure => installed,
}
package { 'alsa-utils':
	provider => 'apt',
	ensure => installed,
}
package { 'gosu':
	ensure   => 'installed',
	provider => 'gem',
        require => Package['libopenal-dev'],
}
file { '/etc/apt/sources.list':
	content => "
deb http://httpredir.debian.org/debian jessie main contrib non-free
deb-src http://httpredir.debian.org/debian jessie main contrib non-free

deb http://security.debian.org/ jessie/updates main
deb-src http://security.debian.org/ jessie/updates main
",
}
exec { 'apt-get-update':
	command     => '/usr/bin/apt-get update',
	refreshonly => true,
	require => File['/etc/apt/sources.list'],
}
$packages = ["libttspico-data", "libttspico-utils"]

package { $packages:
	ensure   => 'installed',
	provider => 'apt',
	require => [
		Exec['apt-get-update'],
	],
}
