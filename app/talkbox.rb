#!/usr/bin/ruby
#

module ZOrder
  BACKGROUND, DECORATION, TEXT, UI = *0..3
end

require 'gosu'

class Talkbox < Gosu::Window
	def initialize
		super 640, 480
		self.caption = "Talkbox"

		@font = Gosu::Font.new(20)
		@background_image = Gosu::Image.new("/opt/play/app/blue.png", :tileable => true)
		@x = @y = 0.0
		@text = ''
	end

	def update
		if Gosu.button_down? Gosu::KB_LEFT or Gosu::button_down? Gosu::GP_LEFT
			@x = @x-1
			if @x < 0
				@x = 0.0
			end
		end
		if Gosu.button_down? Gosu::KB_RIGHT or Gosu::button_down? Gosu::GP_RIGHT
			@x = @x+1
		end
		if Gosu.button_down? Gosu::KB_UP or Gosu::button_down? Gosu::GP_0_UP
			@y = @y-1
			if @y < 0
				@y = 0.0
			end
		end
		if Gosu.button_down? Gosu::KB_DOWN or Gosu::button_down? Gosu::GP_0_DOWN
			@y = @y+1
		end
	end

	def draw
		@background_image.draw(0, 0, 0)
		@font.draw(@text, @x, @y, ZOrder::TEXT, 1.5, 1.5, Gosu::Color::YELLOW)
	end

	def button_down(id)
		if id == Gosu::KB_ESCAPE
			close
		else
			humanchar = ''
			if id > 3 and id < 31
				asciichar = 61+id
				humanchar = asciichar.chr
			end
			if humanchar != ''
				picooutput = `/usr/bin/pico2wave -w /tmp/something.wav #{humanchar}`
				aplayeroutput = `/usr/bin/aplay /tmp/something.wav`
				@text = "#{@text}#{humanchar}"
			end
			super
		end
	end
end

Talkbox.new.show
